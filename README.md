# Saint Martin's University Physics 141-171 Laboratory

Welcome SMU :school: Physics 141-171 Laboratory page :children_crossing:

Here you will find the syllabus, and laboratory write-ups :cool:

## Document portal

This repository serves as a single store for all the documents related to our course.
These are the most up-to-date versions of the laboratory write-ups and other materials.
Typically, each document is stored as its own repository

